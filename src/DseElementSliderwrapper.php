<?php

/**
 * 361GRAD Element Sliderwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementSliderwrapper;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Element Sliderwrapper.
 */
class DseElementSliderwrapper extends Bundle
{
}
