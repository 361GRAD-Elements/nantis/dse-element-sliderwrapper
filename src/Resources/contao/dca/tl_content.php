<?php

/**
 * 361GRAD Element Sliderwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_sliderwrapper_start'] =
    '{type_legend},type,' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests,cssID;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_sliderwrapper'] =
    '{type_legend},type,headline;' .
    '{text_legend},dse_text;' .
    '{image_legend},addImage;' .
    '{sliderteaser_legend},dse_sliderLink,dse_sliderTitle,dse_isNewWindow;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_sliderwrapper_stop']  =
    '{type_legend},type;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

$GLOBALS['TL_DCA']['tl_content']['fields']['url']['eval']['mandatory'] = false;
$GLOBALS['TL_DCA']['tl_content']['fields']['text']['eval']['mandatory'] = false;

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_sliderLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_sliderLink'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_sliderTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_sliderTitle'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isNewWindow'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isNewWindow'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];