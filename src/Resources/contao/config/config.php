<?php

/**
 * 361GRAD Element Sliderwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_sliderwrapper_start'] =
    'Dse\\ElementsBundle\\ElementSliderwrapper\\Element\\ContentDseSliderwrapperStart';

$GLOBALS['TL_CTE']['dse_elements']['dse_sliderwrapper']  =
    'Dse\\ElementsBundle\\ElementSliderwrapper\\Element\\ContentDseSliderwrapper';

$GLOBALS['TL_CTE']['dse_elements']['dse_sliderwrapper_stop']  =
    'Dse\\ElementsBundle\\ElementSliderwrapper\\Element\\ContentDseSliderwrapperStop';

$GLOBALS['TL_WRAPPERS']['start'][] = 'dse_sliderwrapper_start';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'dse_sliderwrapper_stop';
