<?php

/**
 * 361GRAD Element Elementwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']              = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_sliderwrapper_start'] = ['Slider-Wrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_sliderwrapper'] = ['Slider-Wrapper Teaser', ''];
$GLOBALS['TL_LANG']['CTE']['dse_sliderwrapper_stop']  = ['Slider-Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_sliderTitle']  = ['CTA Button Titel', 'Der Linktitel wird auf Hover angezeigt'];
$GLOBALS['TL_LANG']['tl_content']['dse_sliderLink']   = ['CTA Button Link', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['dse_isNewWindow'] = [
    'Ziel',
    'Überprüfen Sie dies, wenn Sie den Link in einem neuen Fenster öffnen möchten.'
];

$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Hintergrundbild', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['sliderteaser_legend']   = 'Teaser-Einstellungen';

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];