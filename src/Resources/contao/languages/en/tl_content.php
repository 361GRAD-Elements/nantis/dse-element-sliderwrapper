<?php

/**
 * 361GRAD Element Elementwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_sliderwrapper_start'] = ['Sliderwrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_sliderwrapper'] = ['Sliderwrapper Teaser', ''];
$GLOBALS['TL_LANG']['CTE']['dse_sliderwrapper_stop'] = ['Sliderwrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_sliderTitle']  = ['CTA Button Title', 'The link title will be displayed on hover.'];
$GLOBALS['TL_LANG']['tl_content']['dse_sliderLink']   = ['CTA Button Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['dse_isNewWindow'] = [
    'Target',
    'Check this if you want to open link in new window.'
];

$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Background Image', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['sliderteaser_legend']   = 'Teaser settings';

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];