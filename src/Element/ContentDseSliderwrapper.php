<?php

/**
 * 361GRAD Element Sliderwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementSliderwrapper\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\File;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseSliderwrapper
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseSliderwrapper extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_sliderwrapper';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {

        $this->Template->addImage = false;

        // Add an image
        if ($this->addImage && $this->singleSRC != '') {
            // Get Files Model of image
            $objModel = FilesModel::findByUuid($this->singleSRC);

            if ($objModel !== null && is_file(TL_ROOT . '/' . $objModel->path)) {
                // Replace singleSRC with image path
                $this->singleSRC = $objModel->path;
                // Add image data into template
                $this->addImageToTemplate($this->Template, $this->arrData);
            }
        }

        // Build subheadline like Contao headline
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';
    }
}
